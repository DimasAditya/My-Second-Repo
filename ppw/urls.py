from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
	url(r'^home$', views.home, name='home2'),
    url(r'^aboutme$', views.aboutme, name='aboutme'),
    url(r'^exp$', views.exp, name='exp'),
	url(r'^guest$', views.guest, name='guest'),
]