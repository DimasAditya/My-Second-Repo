from django.shortcuts import render

def home(request):
    return render(request, 'main.html')

def aboutme(request):
    return render(request, 'aboutme.html')

def exp(request):
    return render(request, 'exp.html')
	
def guest(request):
    return render(request, 'guest.html')